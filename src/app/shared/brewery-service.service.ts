import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BreweryService {

  private apiUrl = environment.apiUrl;
  allBeers: [null];

  constructor(private http: HttpClient) { }

  getBreweries() {
    return this.http.get(this.apiUrl + '/brewer');
  }

  getBeers(brewer: string){
    //brewer = brewer.replace(/\s/g, '');
    console.log('getbeers called', brewer);
    return this.http.get(this.apiUrl + '/beer/get/' + brewer);
  }
}