import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BeerService {

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'text'
    })
  };

  getBeerInfo(beer){
    return this.http.get(environment.untappdUrl + 'search/beer?q=' + beer.brewer + ' ' + beer.beerName + environment.untappdClientInfoSearch);
  }

  getBeerInfoById(beer){
    return this.http.get(environment.untappdUrl + 'beer/info/' + beer.untappdID + environment.untappdClientInfoGet);
    
  }

  addUntappdId(beer){
    return this.http.put(environment.apiUrl + '/addId', beer, { responseType: 'text' });
  }
}