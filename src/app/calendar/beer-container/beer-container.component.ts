import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { BeerService } from '../../shared/beer.service';

@Component({
  selector: 'app-beer-container',
  templateUrl: './beer-container.component.html',
  styleUrls: ['./beer-container.component.scss']
})
export class BeerContainerComponent implements OnChanges {
  @Input() activeBeer: any;
  beerInfo = {};

  constructor(private beerService: BeerService) { }
  
  ngOnChanges(){
    this.getBeerData(this.activeBeer);
  }

  getBeerData(beer){
    console.log('getting beer data');
    if(beer.untappdID){
      this.beerInfo = this.beerService.getBeerInfoById(beer).subscribe((beerData: any) => {
        console.log('got beer with untappd id', beerData);
        this.beerInfo = beerData.response;
      });
    }
    else{
      let i = 0; 
      this.beerInfo = this.beerService.getBeerInfo(beer).subscribe((beerData: any) => {
        console.log('all beer data', beerData);
        while (i < beerData.response.beers.items.length || beerData.response.beers.items[i].beer.beer_name !== beer.beerName) {
          if (beerData.response.beers.items[i].beer.beer_name === beer.beerName && beerData.response.beers.items[i].brewery.brewery_name.indexOf(beer.brewer) > -1) {
            this.beerInfo = beerData.response.beers.items[i].beer;
            console.log('new beer data ', this.beerInfo);
            this.addUntappdID(beerData.response.beers.items[i].beer.bid, beer);
            return;
          }
          i++;
        }
      });
    }
  }

  addUntappdID(id, beer){
    beer.untappdID = id;
    this.beerService.addUntappdId(beer).subscribe();
  }

}
