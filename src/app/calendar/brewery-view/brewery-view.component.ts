import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-brewery-view',
  templateUrl: './brewery-view.component.html',
  styleUrls: ['./brewery-view.component.scss']
})
export class BreweryViewComponent implements OnInit {
  constructor() {}

  @Input() brewery: any;

  ngOnInit() {}
  // @Input()
  // set name(selectedBrewery: string) {
  //   console.log('prev value: ', this.brewery);
  //   console.log('got name: ', this.brewery);
  //   this.brewery = selectedBrewery;
  // }
}
