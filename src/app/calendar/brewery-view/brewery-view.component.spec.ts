import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreweryViewComponent } from './brewery-view.component';

describe('BreweryViewComponent', () => {
  let component: BreweryViewComponent;
  let fixture: ComponentFixture<BreweryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreweryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreweryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
