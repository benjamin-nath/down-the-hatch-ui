import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BreweryService } from '../../shared/brewery-service.service';

@Component({
  selector: 'app-brewery-list',
  templateUrl: './brewery-list.component.html',
  styleUrls: ['./brewery-list.component.scss']
})
export class BreweryListComponent implements OnInit {

  constructor(private breweryService: BreweryService) { }

  breweries: any;
  availableBeers: {};
  activeBeer: {};

  ngOnInit() {
    this.getBreweries();
  }

  getBreweries() {
    this.breweryService.getBreweries().subscribe((data) => {
      this.breweries = data;
    });
  }

  getBeersFromBrewery(event, brewery){
    if(event.source.selected){
      this.breweryService.getBeers(brewery).subscribe((beers)=>{
        this.availableBeers = beers;
      })
    }
  }

  setActiveBeer(beer){
    console.log('selected beer', beer);
    this.activeBeer = beer;
  }

}
