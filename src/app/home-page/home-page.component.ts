import { Component, OnInit } from '@angular/core';
import { MatCardModule } from '@angular/material';

import { BreweryService } from '../shared/brewery-service.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  constructor(private breweryService: BreweryService) {}

  ngOnInit() {}
}
